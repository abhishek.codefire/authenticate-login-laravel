@extends('layouts.app')
@section('content')
<div class="container">
     <div class="row justify-content-center">
         <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                    <div class="card-body">
                       You are successfully logged in!
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                        </div>
                        <img src="uploads/{{ Session::get('photo') }}">
                        @endif
                  
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


               <form action="{{ route('file.upload.post') }}" method="POST" enctype="multipart/form-data">
                @csrf

                 <input type="file" name="file">
                 <button type="submit" class="btn btn-success">Upload</button>
            </form>
           </div>
        </div>
     </div>
    </div>
</div>
@endsection