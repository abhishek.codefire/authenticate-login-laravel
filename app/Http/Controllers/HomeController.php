<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class HomeController extends Controller
{
  public function home()
  {
      return view('home');
  }  

  public function fileUploadPost(Request $request)
  {
    $id = auth()->user()->id;
    $user = User::find($id);
    $image = $request->file('file');
        if($image!=null){
            if(File::exists('uploads/'.$user->photo)) {
                File::delete('uploads/'.$user->photo);
            }
            $imageName=time().".".$image->extension();
            $image->move(public_path('uploads'),$imageName);
            $user->photo=$imageName;
        }
        $user->save();
        return back()->with('success','You have successfully upload file.');
    } 
}