<?php

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Routes;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('register', 'App\Http\Controllers\Auth\RegisterController@register')->name('register');
Route::post('register', 'App\Http\Controllers\Auth\RegisterController@storeUser');

Route::get('login', 'App\Http\Controllers\Auth\LoginController@login')->name('login');
Route::post('login', 'App\Http\Controllers\Auth\LoginController@authenticate');

Route::get('logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::get('home','App\Http\Controllers\HomeController@home');

Route::post('file-upload','App\Http\Controllers\HomeController@fileUploadPost')->name('file.upload.post');

Route::get('forget-password', 'App\Http\Controllers\Auth\ForgotPasswordController@getEmail')->name('forget-password');
Route::post('forget-password', 'App\Http\Controllers\Auth\ForgotPasswordController@postEmail');

Route::get('reset-password/{token}', 'App\http\controllers\auth\resetpasswordcontroller@getpassword')->name('reset.password');
Route::post('reset-password', 'App\Http\Controllers\Auth\ResetPasswordController@updatePassword');